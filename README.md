
## References
1. https://money.stackexchange.com/questions/125156/can-a-market-order-be-matched-by-another-market-order-in-stock-trading
2. https://github.com/Kautenja/limit-order-book/blob/master/notes/lob.md
3. https://github.com/exchange-core/exchange-core
4. https://www.investopedia.com/terms/p/premarket.asp
5. https://web.archive.org/web/20110219163448/http://howtohft.wordpress.com/2011/02/15/how-to-build-a-fast-limit-order-book/
6. https://github.com/OpenHFT/Chronicle-Queue

   Pre-market trading can only be executed with limited orders through an "electronic market" 
   like an alternative trading system (ATS) or electronic communication network (ECN). 
   Market makers are not permitted to execute orders until the 9:30 a.m. EST opening bell.

7. https://www.tickertape.in/blog/pre-open-market/
   
   Generally, based on the demand and supply mechanism, the equilibrium price is the price at which the maximum volume is executable. 
   The equilibrium price is based on both the limit orders and the market orders. It is considered as the open price for the subsequent continuous trading day.
   Unmatched/unexecuted limit orders and market orders during the pre-open session will be moved to the following trading session at the same price.


8. https://gist.github.com/gabhi/97d9182ea8df09c1e389


## Usage
mvn clean install

### preopen stage
java -jar *base folder*/target/Simulator-1.0-SNAPSHOT-jar-with-dependencies.jar -pre /home/viktor/work/luxoft/java-056_java_core_practical_seminar/src/test/resources/preopening/

### all stages
java -jar target/Simulator-1.0-SNAPSHOT-jar-with-dependencies.jar \
-pre *base folder*/preopening \
-cont *base folder*/continuous \
-post *base folder*/preclosing
