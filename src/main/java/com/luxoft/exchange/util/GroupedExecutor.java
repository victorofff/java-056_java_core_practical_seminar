package com.luxoft.exchange.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class GroupedExecutor {

    private static final long SHUTDOWN_SEC = 10;

    private final Map<String, ExecutorService> tasksMap = new ConcurrentHashMap<>();
    private final List<ExecutorService> executors = new ArrayList<>();
    private volatile boolean stopping;

    public synchronized void init(Set<String> securities, int threadNum, int queueCapacity) {
        if (stopping) {
            throw new IllegalArgumentException("In running mode");
        }

        executors.clear();
        tasksMap.clear();

        for (int n = 0; n < threadNum; n++) {

            // just to have more control on the queue
            ExecutorService executor = new ThreadPoolExecutor(1, 1,
                0L, TimeUnit.MILLISECONDS,
                new ArrayBlockingQueue<>(queueCapacity));

            executors.add(executor);
        }

        for (String security : securities) {
            int hashCode = security.hashCode();
            int bucket = hashCode % threadNum;

            tasksMap.computeIfAbsent(security, (k) -> executors.get(bucket));
            if (log.isDebugEnabled()) {
                log.debug("Symbol {} is assigned to the executor #{}", security, bucket);
            }
        }

    }


    public synchronized void shutdown() {
        stopping = true;
        for (ExecutorService service : executors) {
            GeneralUtil.shutdownExecutorService(service, SHUTDOWN_SEC);
        }

        executors.clear();
        tasksMap.clear();
        stopping = false;
    }

    public void addRunnable(String symbol, Runnable runnable) {
        if (stopping) {
            throw new IllegalArgumentException("Service already flagged as stopped");
        }
        ExecutorService executorService = tasksMap.get(symbol);
        if (executorService == null) {
            throw new IllegalArgumentException("Invalid executor service for a symbol: " + symbol);
        }
        executorService.submit(runnable);

    }

}
