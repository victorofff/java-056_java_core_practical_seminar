package com.luxoft.exchange.util;

import com.google.common.util.concurrent.Uninterruptibles;
import com.luxoft.exchange.simulator.orderbook.OrderFactory;
import com.luxoft.exchange.simulator.orders.Order;
import com.luxoft.exchange.simulator.orders.OrderOperation;
import com.luxoft.exchange.simulator.orders.OrderType;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;

@UtilityClass
@Slf4j
public class GeneralUtil {


    public static void shutdownExecutorService(ExecutorService service, long shutdownSecs) {
        service.shutdown();
        boolean shutDownOk = Uninterruptibles.awaitTerminationUninterruptibly(service, shutdownSecs,
            TimeUnit.SECONDS);
        if (!shutDownOk) {
            service.shutdownNow();
            log.warn("Could not gracefully shutdown executor service");
        } else {
            if (log.isDebugEnabled()) {
                log.debug("Successfully shutdown executor service");
            }
        }
    }

    public static List<File> readFilesFromDirectory(String directory) throws IOException {
        List<File> result = new ArrayList<>();
        try (Stream<Path> paths = Files.walk(Paths.get(directory))) {
            paths.filter(Files::isRegularFile)
                .filter(file -> FilenameUtils.getExtension(file.getFileName().toString()).equalsIgnoreCase("txt"))
                .forEach(file -> result.add(file.toFile()));
        }

        return result;
    }

    public static Order createRegularOrder(String[] row) {

        if (row == null || (row.length != 4 && row.length != 5)) {
            throw new IllegalArgumentException("Invalid regular order row");
        }

        sanitizeRow(row);

        String security = row[0];
        OrderOperation operation = row[1].startsWith("B") ? OrderOperation.BUY : OrderOperation.SELL;
        OrderType type = row[3].startsWith("M") ? OrderType.MARKET : OrderType.LIMIT;
        int size = Integer.parseInt(row[2]);

        return type == OrderType.MARKET ? OrderFactory.createMarkerOrder(LocalDateTime.now(), security, operation, size) :
            OrderFactory.createLimitOrder(LocalDateTime.now(), security, operation, BigDecimal.valueOf(Double.parseDouble(row[4])), size);

    }


    public static Order createPrePostOrder(String[] row) {
        if (row == null || row.length != 4) {
            throw new IllegalArgumentException("Invalid pre-post order row");
        }

        sanitizeRow(row);

        String security = row[0];
        BigDecimal price = BigDecimal.valueOf(Double.parseDouble(row[3]));
        int size = Integer.parseInt(row[2]);
        OrderOperation operation = row[1].equals("B") ? OrderOperation.BUY : OrderOperation.SELL;

        return OrderFactory.createLimitOrder(LocalDateTime.now(), security, operation, price, size);
    }

    private static void sanitizeRow(String[] row) {
        for (String e : row) {
            if (StringUtils.isEmpty(e)) {
                throw new IllegalArgumentException("Invalid data");
            }
        }
    }


}
