package com.luxoft.exchange;

import static com.luxoft.exchange.util.GeneralUtil.readFilesFromDirectory;

import com.google.common.util.concurrent.Uninterruptibles;
import com.luxoft.exchange.simulator.orderbook.FixRecord;
import com.luxoft.exchange.simulator.orderbook.OrderBook;
import com.luxoft.exchange.simulator.orderbook.OrderBookImpl;
import com.luxoft.exchange.simulator.orderbook.Trade;
import com.luxoft.exchange.simulator.orders.Order;
import com.luxoft.exchange.util.GeneralUtil;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.io.Reader;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

@Slf4j
@AllArgsConstructor
public class Simulator {

    private static final String PRE = "pre";
    private static final String CONT = "cont";
    private static final String POST = "post";

    private static final Consumer<Trade> DEFAULT_TRADE_CONSUMER = trade -> {
        log.info("Trade arrived: {}", trade);
    };


    /*package*/
    final Map<String, OrderBook> orderBookMap = new ConcurrentHashMap<>();

    private final Consumer<Trade> tradeConsumer;

    private final ExecutorService readFilesExecutorService = new ThreadPoolExecutor(
        0, Integer.MAX_VALUE, 0, TimeUnit.SECONDS, new SynchronousQueue<>());

    private final ExecutorService fixOutMarketExecutorService = Executors.newWorkStealingPool();


    public static void main(String[] args) {
        try {
            Options options = new Options();

            options.addOption(Option.builder(PRE)
                .required(false)
                .hasArg(true)
                .longOpt("premarket")
                .desc("Pre-session folder")
                .build());

            options.addOption(Option.builder(POST)
                .required(false)
                .hasArg(true)
                .longOpt("postmarket")
                .desc("Post-session folder")
                .build());

            options.addOption(Option.builder(CONT)
                .required(false)
                .hasArg(true)
                .longOpt("continuous")
                .desc("Session folder")
                .build());

            CommandLineParser parser = new DefaultParser();
            CommandLine cmd = parser.parse(options, args);

            Simulator simulator = new Simulator(DEFAULT_TRADE_CONSUMER);

            boolean processed = false;
            if (cmd.hasOption(PRE)) {
                logFixRecords(simulator.outMarketSimulation(cmd.getOptionValue(PRE)));
                processed = true;
            }

            if (cmd.hasOption(CONT)) {
                simulator.marketSimulation(cmd.getOptionValue(CONT));
                processed = true;
            }

            if (cmd.hasOption(POST)) {
                logFixRecords(simulator.outMarketSimulation(cmd.getOptionValue(POST)));
                processed = true;
            }

            if (!processed) {
                log.warn("No processing happened");
            }


        } catch (ParseException | IOException | ExecutionException exception) {
            log.error("Parse argument exception happened during simulation", exception);
        } catch (RuntimeException runtimeException) {
            log.error("Exception happened during simulation", runtimeException);
        }

    }

    public synchronized List<FixRecord> outMarketSimulation(String sourceDirectory) throws IOException, ExecutionException {

        log.info("Started out of market simulation in directory {}", sourceDirectory);
        readFilesConcurrently(sourceDirectory, GeneralUtil::createPrePostOrder, this::insertOrderForDelayedProcessing);

        List<CompletableFuture<FixRecord>> preopenProcessFutures = orderBookMap
            .values().stream().map(
                ob -> CompletableFuture.supplyAsync(ob::fixAllOrders, fixOutMarketExecutorService)).toList();

        CompletableFuture<List<FixRecord>> completableFixRecordFuture = CompletableFuture
            .allOf(preopenProcessFutures.toArray(new CompletableFuture[0]))
            .thenApply(future -> preopenProcessFutures.stream()
                .map(CompletableFuture::join)
                .collect(Collectors.toList()));

        List<FixRecord> result = Uninterruptibles.getUninterruptibly(completableFixRecordFuture);
        log.info("Finished out of marked simulation in directory {}", sourceDirectory);
        return result;
    }

    public synchronized void marketSimulation(String sourceDirectory) throws IOException {

        log.info("Started session simulation in directory {}", sourceDirectory);
        Set<String> securities = resolveAllSymbols(sourceDirectory, false);
        securities.forEach(symbol -> resolveOrderBookEntry(symbol).startSessionProcessing(securities));

        try {
            readFilesConcurrently(sourceDirectory, GeneralUtil::createRegularOrder, this::insertOrderForImmediateProcessing);
        } finally {
            securities.forEach(symbol -> resolveOrderBookEntry(symbol).finishSessionProcessing());
        }
        log.info("Finished session simulation in directory {}", sourceDirectory);

    }

    /*package*/
    void readFilesConcurrently(String sourceDirectory,
        Function<String[], Order> orderMapper,
        Consumer<Order> orderConsumer) throws IOException {
        List<File> files = readFilesFromDirectory(sourceDirectory);

        // reading files in per file dedicated thread mode
        List<CompletableFuture<Void>> preopenReadFileFutures = files.stream()
            .map(file -> CompletableFuture.runAsync(() -> parseRecordsLines(file,
                    row -> orderConsumer.accept(orderMapper.apply(row))),
                readFilesExecutorService)).toList();

        CompletableFuture.allOf(preopenReadFileFutures.toArray(new CompletableFuture[0])).join();

    }

    /*package*/
    Set<String> resolveAllSymbols(String sourceDirectory, boolean isPreSession) throws IOException {
        Set<String> result = new HashSet<>();
        List<File> files = readFilesFromDirectory(sourceDirectory);

        for (File file : files) {
            parseRecordsLines(file, row -> {
                Order order = isPreSession ? GeneralUtil.createPrePostOrder(row) : GeneralUtil.createRegularOrder(row);
                result.add(order.getSecurity());
            });
        }
        return result;
    }

    private OrderBook resolveOrderBookEntry(String security) {
        return orderBookMap.computeIfAbsent(security, s -> {
            OrderBook ob = new OrderBookImpl(security);
            ob.init(tradeConsumer == null ? DEFAULT_TRADE_CONSUMER : tradeConsumer);
            return ob;
        });
    }

    private void insertOrderForDelayedProcessing(Order order) {
        if (log.isDebugEnabled()) {
            log.debug("Inserting order {}", order);
        }
        resolveOrderBookEntry(order.getSecurity()).enqueueOrderForDelayedProcessing(order);
    }

    private void insertOrderForImmediateProcessing(Order order) {
        if (log.isDebugEnabled()) {
            log.debug("Processing order {}", order);
        }
        resolveOrderBookEntry(order.getSecurity()).enqueueOrderForImmediateProcessing(order);
    }

    /*package*/
    static void parseRecordsLines(File file, Consumer<String[]> consumer) {
        try (Reader rd = new FileReader(file);
            Reader br = new BufferedReader(rd);
            LineNumberReader lineNumberReader = new LineNumberReader(br);
        ) {
            String line;
            while ((line = lineNumberReader.readLine()) != null) {
                String[] elements = line.split(";");
                consumer.accept(elements);
            }
        } catch (IOException ioException) {
            throw new IllegalArgumentException("File processing exception: " + file.getPath(), ioException);
        }
    }


    private static void logFixRecords(List<FixRecord> fixRecords) {
        fixRecords.forEach(fr -> log.info("Fix record: [{}]", fr));
    }


}
