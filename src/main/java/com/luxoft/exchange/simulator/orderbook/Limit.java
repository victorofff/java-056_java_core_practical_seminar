package com.luxoft.exchange.simulator.orderbook;

import com.luxoft.exchange.simulator.orderbook.Trade.State;
import com.luxoft.exchange.simulator.orders.OrderImpl;
import java.math.BigDecimal;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.function.Consumer;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class Limit {

    private final BigDecimal limitPrice;
    private int totalVolume;

    /*package*/
    final Map<Integer, OrderImpl> orders = new LinkedHashMap<>();

    public OrderImpl remove(int orderId) {
        OrderImpl removed = orders.remove(orderId);
        if (removed == null) {
            throw new IllegalArgumentException("Invalid order id: " + orderId);
        }

        totalVolume -= removed.getRemainingSize();
        return removed;
    }

    public void put(OrderImpl order) {
        orders.put(order.getOrderId(), order);
        totalVolume += order.getRemainingSize();
    }

    // tries to match and returns remaining volume, if necessary removes orders
    public int matchToOrder(OrderImpl sourceOrder, Consumer<Trade> tradeConsumer) {
        int volumeToFill = sourceOrder.getRemainingSize();
        for (Iterator<Map.Entry<Integer, OrderImpl>> it = orders.entrySet().iterator(); it.hasNext(); ) {
            OrderImpl oppositeOrder = it.next().getValue();

            int newVolume = volumeToFill - oppositeOrder.getRemainingSize();
            if (newVolume > 0) {

                sourceOrder.setRemainingSize(sourceOrder.getRemainingSize() - oppositeOrder.getRemainingSize());

                // source is not covered, from the limit is covered
                Trade sourceTrade = new Trade(sourceOrder.getSecurity(), State.PARTIALLY_FILLED, sourceOrder.getOrderOperation(), sourceOrder.getOrderId(),
                    limitPrice, oppositeOrder.getRemainingSize());
                tradeConsumer.accept(sourceTrade);

                Trade targetTrade = new Trade(sourceOrder.getSecurity(), State.FILLED, oppositeOrder.getOrderOperation(), oppositeOrder.getOrderId(),
                    limitPrice, oppositeOrder.getRemainingSize());
                tradeConsumer.accept(targetTrade);

                it.remove();
                totalVolume -= oppositeOrder.getRemainingSize();
                volumeToFill -= oppositeOrder.getRemainingSize();

            } else if (newVolume == 0) {

                sourceOrder.setRemainingSize(0);

                // source is covered, from the limit is covered
                Trade sourceTrade = new Trade(sourceOrder.getSecurity(), State.FILLED, sourceOrder.getOrderOperation(), sourceOrder.getOrderId(),
                    limitPrice, oppositeOrder.getRemainingSize());
                tradeConsumer.accept(sourceTrade);

                Trade targetTrade = new Trade(sourceOrder.getSecurity(), State.FILLED, oppositeOrder.getOrderOperation(), oppositeOrder.getOrderId(),
                    limitPrice, oppositeOrder.getRemainingSize());
                tradeConsumer.accept(targetTrade);

                it.remove();
                totalVolume -= oppositeOrder.getRemainingSize();
                volumeToFill = 0;
                break;
            } else {

                // source is covered, limit is partially filled
                Trade sourceTrade = new Trade(sourceOrder.getSecurity(), State.FILLED, sourceOrder.getOrderOperation(), sourceOrder.getOrderId(),
                    limitPrice, sourceOrder.getRemainingSize());

                tradeConsumer.accept(sourceTrade);

                oppositeOrder.setRemainingSize(oppositeOrder.getRemainingSize() - volumeToFill);
                totalVolume -= sourceOrder.getRemainingSize();

                Trade targetTrade = new Trade(sourceOrder.getSecurity(), State.PARTIALLY_FILLED, oppositeOrder.getOrderOperation(), oppositeOrder.getOrderId(),
                    limitPrice, sourceOrder.getRemainingSize());
                tradeConsumer.accept(targetTrade);

                sourceOrder.setRemainingSize(0);
                volumeToFill = 0;
                break;

            }
        }

        return volumeToFill;
    }

}
