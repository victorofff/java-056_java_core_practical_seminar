package com.luxoft.exchange.simulator.orderbook;

import java.math.BigDecimal;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

public record FixRecord(String security, BigDecimal fixPrice, int fixVolume) {
}
