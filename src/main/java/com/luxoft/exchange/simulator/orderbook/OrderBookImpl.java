package com.luxoft.exchange.simulator.orderbook;

import com.luxoft.exchange.simulator.orderbook.Trade.State;
import com.luxoft.exchange.simulator.orders.Order;
import com.luxoft.exchange.simulator.orders.OrderImpl;
import com.luxoft.exchange.simulator.orders.OrderOperation;
import com.luxoft.exchange.util.GroupedExecutor;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.NavigableMap;
import java.util.Queue;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.function.Consumer;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RequiredArgsConstructor
@Slf4j
public class OrderBookImpl implements OrderBook {

    private static final int SECURITY_QUEUE_CAPACITY = 10000;

    private final String security;

    private Consumer<Trade> tradeConsumer;

    /*package*/
    final Map<Integer, OrderImpl> orders = new HashMap<>();

    /*package*/
    final Map<BigDecimal, Limit> limitDictSells = new HashMap<>();

    /*package*/
    final Map<BigDecimal, Limit> limitDictBuys = new HashMap<>();

    /*package*/
    final NavigableMap<BigDecimal, Limit> sells = new TreeMap<>();

    /*package*/
    final NavigableMap<BigDecimal, Limit> buys = new TreeMap<>();

    private final Queue<OrderImpl> delayedOrderQueue = new ConcurrentLinkedQueue<>();

    private final GroupedExecutor groupedExecutor = new GroupedExecutor();

    @Override
    public void init(Consumer<Trade> tradeConsumer) {
        this.tradeConsumer = tradeConsumer;
    }

    // expected to be called from multiple threads - but a thread is dedicated to a security
    @Override
    public void enqueueOrderForImmediateProcessing(Order order) {
        groupedExecutor.addRunnable(order.getSecurity(), () -> {
            try {
                processOrderImmediately(order);
            } catch (Throwable throwable) {
                log.error("Error on order processing", throwable);
            }
        });

    }

    @Override
    public void startSessionProcessing(Set<String> securities) {
        // Runtime.getRuntime().availableProcessors() - must be adjusted according to counterpart writers
        groupedExecutor.init(securities, Runtime.getRuntime().availableProcessors(), SECURITY_QUEUE_CAPACITY);
    }


    @Override
    public void finishSessionProcessing() {
        groupedExecutor.shutdown();
    }

    // expected to be called from multiple threads
    @Override
    public void enqueueOrderForDelayedProcessing(Order order) {
        if (log.isDebugEnabled()) {
            log.debug("Enqueuing order for delayed processing: {}", order);
        }
        OrderImpl orderImpl = (OrderImpl) order;
        delayedOrderQueue.add(orderImpl);
    }

    // expected to be called from a single thread per LOB
    @Override
    public FixRecord fixAllOrders() {
        log.info("Fixing all orders");
        Consumer<Trade> oldConsumer = tradeConsumer;

        BigDecimal[] maxPrice = new BigDecimal[]{BigDecimal.valueOf(-1)};
        int[] maxVolume = new int[]{-1};

        tradeConsumer = trade -> {
            if (oldConsumer != null) {
                oldConsumer.accept(trade);
            }

            if (trade.isFilled()) {
                if (maxVolume[0] < trade.getFilledVolume()) {
                    maxPrice[0] = trade.getPrice();
                    maxVolume[0] = trade.getFilledVolume();
                }
            }
        };

        try {
            while (true) {
                Order order = delayedOrderQueue.poll();
                if (order == null) {
                    break;
                }
                processOrderImmediately(order);
            }

        } finally {
            tradeConsumer = oldConsumer;
        }

        return new FixRecord(security, maxPrice[0], maxVolume[0]);
    }

    @Override
    public void cancel(int orderId) {
        OrderImpl removed = orders.remove(orderId);
        if (removed == null) {
            throw new IllegalArgumentException("Order does not exist: " + orderId);
        }

        Limit parentLimit = removed.getParentLimit();
        removed = parentLimit.remove(removed.getOrderId());

        if (parentLimit.getTotalVolume() == 0) {
            NavigableMap<BigDecimal, Limit> bucket = selectBucketByOperation(removed.getOrderOperation());
            bucket.remove(removed.getPrice());

            Map<BigDecimal, Limit> limitDict = selectLimitDictByOperation(removed.getOrderOperation());
            limitDict.remove(removed.getPrice());
        }

    }

    /*package*/
    void processOrderImmediately(Order order) {
        OrderImpl orderImpl = (OrderImpl) order;
        if (log.isDebugEnabled()) {
            log.debug("Processing order immediately: {}", order);
        }
        switch (order.getOrderType()) {
            case MARKET -> processMarketOrder(orderImpl);
            case LIMIT -> processLimitOrder(orderImpl);
        }
    }


    private void processLimitOrder(OrderImpl order) {

        int volumeToFill = order.getSize();
        OrderOperation oppositeOperation = order.getOrderOperation() == OrderOperation.BUY ? OrderOperation.SELL : OrderOperation.BUY;
        NavigableMap<BigDecimal, Limit> oppositeMap = selectBucketByOperation(oppositeOperation);

        for (Iterator<Map.Entry<BigDecimal, Limit>> it = oppositeMap.entrySet().iterator(); it.hasNext(); ) {
            Limit nextLimit = it.next().getValue();

            boolean transactionCondition = order.getOrderOperation().equals(OrderOperation.BUY)
                ? order.getPrice().compareTo(nextLimit.getLimitPrice()) >= 0
                : order.getPrice().compareTo(nextLimit.getLimitPrice()) <= 0;

            if (!transactionCondition) {
                break;
            }

            volumeToFill = nextLimit.matchToOrder(order, tradeConsumer);
            if (nextLimit.getTotalVolume() == 0) {
                it.remove();

                Map<BigDecimal, Limit> limitDict = selectLimitDictByOperation(oppositeOperation);
                limitDict.remove(order.getPrice());
            }

            if (volumeToFill == 0) {
                break;
            }

        }

        if (volumeToFill > 0) {
            // need to install the remaining part
            installLimitOrder(order);
        }
    }

    private void installLimitOrder(OrderImpl order) {
        Map<BigDecimal, Limit> limitDict = selectLimitDictByOperation(order.getOrderOperation());
        Limit limit = limitDict.get(order.getPrice());
        if (limit == null) {
            limit = new Limit(order.getPrice());

            NavigableMap<BigDecimal, Limit> limitMap = selectBucketByOperation(order.getOrderOperation());
            limitMap.put(limit.getLimitPrice(), limit);
            limitDict.put(limit.getLimitPrice(), limit);
        }

        order.setParentLimit(limit);
        limit.put(order);
        orders.put(order.getOrderId(), order);

        Trade trade = new Trade(order.getSecurity(), State.INSTALLED, order.getOrderOperation(), order.getOrderId(), order.getPrice(), -1);
        tradeConsumer.accept(trade);

    }

    private void processMarketOrder(OrderImpl order) {
        int volumeToFill = order.getSize();
        OrderOperation oppositeOperation = order.getOrderOperation() == OrderOperation.BUY ? OrderOperation.SELL : OrderOperation.BUY;
        NavigableMap<BigDecimal, Limit> oppositeMap = selectBucketByOperation(oppositeOperation);

        for (Iterator<Map.Entry<BigDecimal, Limit>> it = oppositeMap.entrySet().iterator(); it.hasNext(); ) {
            Limit nextLimit = it.next().getValue();

            volumeToFill = nextLimit.matchToOrder(order, tradeConsumer);
            if (nextLimit.getTotalVolume() == 0) {
                it.remove();

                Map<BigDecimal, Limit> limitDict = selectLimitDictByOperation(oppositeOperation);
                limitDict.remove(order.getPrice());
            }
            if (volumeToFill == 0) {
                break;
            }
        }

        if (volumeToFill > 0) {
            Trade trade = new Trade(order.getSecurity(), State.NOT_FILLED, order.getOrderOperation(), order.getOrderId(), BigDecimal.valueOf(-1),
                volumeToFill);
            tradeConsumer.accept(trade);
        }
    }


    private NavigableMap<BigDecimal, Limit> selectBucketByOperation(OrderOperation orderOperation) {
        if (orderOperation == OrderOperation.BUY) {
            return buys;
        } else {
            return sells;
        }
    }

    private Map<BigDecimal, Limit> selectLimitDictByOperation(OrderOperation orderOperation) {
        if (orderOperation == OrderOperation.BUY) {
            return limitDictBuys;
        } else {
            return limitDictSells;
        }
    }


}
