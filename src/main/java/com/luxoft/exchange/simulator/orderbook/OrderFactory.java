package com.luxoft.exchange.simulator.orderbook;

import com.luxoft.exchange.simulator.orders.Order;
import com.luxoft.exchange.simulator.orders.OrderImpl;
import com.luxoft.exchange.simulator.orders.OrderOperation;
import com.luxoft.exchange.simulator.orders.OrderType;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import lombok.experimental.UtilityClass;

@UtilityClass
public class OrderFactory {
    private static final BigDecimal INVALID_PRICE = BigDecimal.valueOf(-1.0);

    public static Order createMarkerOrder(LocalDateTime orderTime, String security, OrderOperation operation, int size) {
        OrderImpl order = new OrderImpl(orderTime, security, OrderType.MARKET, operation, INVALID_PRICE, size);
        order.setRemainingSize(size);
        return order;
    }

    public static Order createLimitOrder(LocalDateTime orderTime, String security, OrderOperation operation, BigDecimal price, int size) {
        OrderImpl order = new OrderImpl(orderTime, security, OrderType.LIMIT, operation, price, size);
        order.setRemainingSize(size);
        return order;
    }

    public static Order createLimitOrder(LocalDateTime orderTime, String security, OrderOperation operation, double price, int size) {
        OrderImpl order = new OrderImpl(orderTime, security, OrderType.LIMIT, operation, BigDecimal.valueOf(price), size);
        order.setRemainingSize(size);
        return order;
    }
}
