package com.luxoft.exchange.simulator.orderbook;

import com.luxoft.exchange.simulator.orders.OrderOperation;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import lombok.Data;
import lombok.ToString;

@Data
@ToString(exclude = "tid")
public class Trade {
    public enum State {
        INSTALLED,
        FILLED,
        PARTIALLY_FILLED,
        NOT_FILLED
    }

    public boolean isFilled() {
        return state == State.FILLED || state == State.PARTIALLY_FILLED;
    }

    private final String security;
    private final State state;
    private final OrderOperation operation;
    private final int orderId;
    private final BigDecimal price;
    private final int filledVolume;
    private final LocalDateTime dateTime = LocalDateTime.now();
    private final long tid = Thread.currentThread().getId();
}
