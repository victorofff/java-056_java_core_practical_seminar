package com.luxoft.exchange.simulator.orderbook;

import com.luxoft.exchange.simulator.orders.Order;
import java.util.Set;
import java.util.function.Consumer;

public interface OrderBook {

    void enqueueOrderForImmediateProcessing(Order order);

    void startSessionProcessing(Set<String> securities);

    void finishSessionProcessing();

    void enqueueOrderForDelayedProcessing(Order order);

    FixRecord fixAllOrders();

    void cancel(int orderId);

    void init(Consumer<Trade> tradeConsumer);


}
