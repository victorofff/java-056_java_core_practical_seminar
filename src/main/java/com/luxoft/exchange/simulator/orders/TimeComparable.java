package com.luxoft.exchange.simulator.orders;

public interface TimeComparable<T> {

    int checkEarlier(TimeComparable<T> another);
}
