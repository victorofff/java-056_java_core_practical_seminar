package com.luxoft.exchange.simulator.orders;

import com.luxoft.exchange.simulator.orderbook.Limit;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.concurrent.atomic.AtomicInteger;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@RequiredArgsConstructor
@Setter
@Getter
@ToString(exclude = {"parentLimit", "remainingSize"})
@EqualsAndHashCode(of = {"security", "type", "operation", "price", "size"})
public class OrderImpl implements Order {

    private static final AtomicInteger CUR_ID = new AtomicInteger(0);

    private final int id = CUR_ID.incrementAndGet();
    private final LocalDateTime dateTime;
    private final String security;
    private final OrderType type;
    private final OrderOperation operation;
    private final BigDecimal price;
    private final int size;

    private int remainingSize;
    private Limit parentLimit;


    @Override
    public LocalDateTime getOrderDateTime() {
        return dateTime;
    }

    @Override
    public String getSecurity() {
        return security;
    }

    @Override
    public int getOrderId() {
        return id;
    }

    @Override
    public OrderType getOrderType() {
        return type;
    }

    @Override
    public OrderOperation getOrderOperation() {
        return operation;
    }

    public int getSize() {
        return size;
    }

    @Override
    public BigDecimal getPrice() {
        return price;
    }

    @Override
    public int checkEarlier(TimeComparable<Order> another) {
        if (another instanceof Order anotherOrder) {
            return dateTime.compareTo(anotherOrder.getOrderDateTime());
        } else {
            throw new IllegalArgumentException("Cannot compare incompatible types");
        }

    }
}
