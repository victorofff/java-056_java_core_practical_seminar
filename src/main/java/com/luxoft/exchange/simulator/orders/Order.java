package com.luxoft.exchange.simulator.orders;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public interface Order extends TimeComparable<Order> {

    LocalDateTime getOrderDateTime();

    String getSecurity();

    int getOrderId();

    OrderType getOrderType();

    OrderOperation getOrderOperation();

    int getSize();

    BigDecimal getPrice();
}
