package com.luxoft.exchange.simulator.orders;

public enum OrderOperation {
    BUY,
    SELL;
}
