package com.luxoft.exchange.simulator.orders;

public enum OrderType {
    MARKET,
    LIMIT;
}
