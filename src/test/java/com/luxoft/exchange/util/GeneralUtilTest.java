package com.luxoft.exchange.util;

import static org.assertj.core.api.Assertions.assertThat;

import com.luxoft.exchange.simulator.orders.Order;
import com.luxoft.exchange.simulator.orders.OrderOperation;
import com.luxoft.exchange.simulator.orders.OrderType;
import java.math.BigDecimal;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class GeneralUtilTest {

    @Test
    public void createInvalidPrePostOrderTest() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            GeneralUtil.createPrePostOrder(new String[]{"SNP", "B", "500", "1.25", "1.26"});
        });
    }

    @Test
    public void createPrePostOrderVanillaTest() {
        Order order = GeneralUtil.createPrePostOrder(new String[]{"SNP", "B", "500", "1.25"});
        assertThat(order.getPrice()).isEqualTo(BigDecimal.valueOf(1.25));
        assertThat(order.getSize()).isEqualTo(500);
        assertThat(order.getSecurity()).isEqualTo("SNP");
        assertThat(order.getOrderType()).isEqualTo(OrderType.LIMIT);
        assertThat(order.getOrderOperation()).isEqualTo(OrderOperation.BUY);
    }


    @Test
    public void createRegularOrderLimitVanillaTest() {
        Order order = GeneralUtil.createRegularOrder(new String[]{"SNN", "Sell", "3000", "Limit", "1.27"});
        assertThat(order.getPrice()).isEqualTo(BigDecimal.valueOf(1.27));
        assertThat(order.getSize()).isEqualTo(3000);
        assertThat(order.getSecurity()).isEqualTo("SNN");
        assertThat(order.getOrderType()).isEqualTo(OrderType.LIMIT);
        assertThat(order.getOrderOperation()).isEqualTo(OrderOperation.SELL);
    }

    @Test
    public void createRegularOrderMarketVanillaTest() {
        Order order = GeneralUtil.createRegularOrder(new String[]{"SNN", "Buy", "3010", "Market"});
        assertThat(order.getPrice()).isEqualTo(BigDecimal.valueOf(-1.0));
        assertThat(order.getSize()).isEqualTo(3010);
        assertThat(order.getSecurity()).isEqualTo("SNN");
        assertThat(order.getOrderType()).isEqualTo(OrderType.MARKET);
        assertThat(order.getOrderOperation()).isEqualTo(OrderOperation.BUY);
    }

    @Test
    public void createInvalidRegularOrderMarketTest() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            GeneralUtil.createPrePostOrder(new String[]{"", "Buy", "3010", "Market"});
        });
    }


}