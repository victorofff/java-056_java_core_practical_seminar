package com.luxoft.exchange.simulator.orderbook;


import static org.assertj.core.api.Assertions.assertThat;

import com.luxoft.exchange.simulator.orderbook.Trade.State;
import com.luxoft.exchange.simulator.orders.Order;
import com.luxoft.exchange.simulator.orders.OrderOperation;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


class OrderImplBookTest {

    private static final String SECURITY = "DUMMY";

    private final List<Trade> trades = new ArrayList<>();

    private OrderBookImpl orderBook;

    private Order orderBuy1;
    private Order orderBuy2;
    private Order orderBuy3;
    private Order orderBuy4;
    private Order orderBuy5;
    private Order orderBuy6;

    private Order orderSell1;
    private Order orderSell2;
    private Order orderSell3;
    private Order orderSell4;
    private Order orderSell5;
    private Order orderSell6;

    @BeforeEach
    void init() {
        trades.clear();
        orderBook = new OrderBookImpl(SECURITY);
        Consumer<Trade> tradeConsumer = trades::add;
        orderBook.init(tradeConsumer);

        orderBuy1 = OrderFactory.createLimitOrder(LocalDateTime.now(), SECURITY, OrderOperation.BUY, 1.2520, 10);
        orderBuy2 = OrderFactory.createLimitOrder(LocalDateTime.now(), SECURITY, OrderOperation.BUY, 1.2510, 200);
        orderBuy3 = OrderFactory.createLimitOrder(LocalDateTime.now(), SECURITY, OrderOperation.BUY, 1.2501, 350);
        orderBuy4 = OrderFactory.createLimitOrder(LocalDateTime.now(), SECURITY, OrderOperation.BUY, 1.2480, 14000);
        orderBuy5 = OrderFactory.createLimitOrder(LocalDateTime.now(), SECURITY, OrderOperation.BUY, 1.2370, 15);
        orderBuy6 = OrderFactory.createLimitOrder(LocalDateTime.now(), SECURITY, OrderOperation.BUY, 1.2221, 205);

        orderSell1 = OrderFactory.createLimitOrder(LocalDateTime.now(), SECURITY, OrderOperation.SELL, 1.2630, 15);
        orderSell2 = OrderFactory.createLimitOrder(LocalDateTime.now(), SECURITY, OrderOperation.SELL, 1.2640, 220);
        orderSell3 = OrderFactory.createLimitOrder(LocalDateTime.now(), SECURITY, OrderOperation.SELL, 1.2645, 35);
        orderSell4 = OrderFactory.createLimitOrder(LocalDateTime.now(), SECURITY, OrderOperation.SELL, 1.2655, 14);
        orderSell5 = OrderFactory.createLimitOrder(LocalDateTime.now(), SECURITY, OrderOperation.SELL, 1.2701, 15000);
        orderSell6 = OrderFactory.createLimitOrder(LocalDateTime.now(), SECURITY, OrderOperation.SELL, 1.2890, 205000);
    }

    @Test
    public void testFixRecordsTest() {
        Order orderBuy1 = OrderFactory.createLimitOrder(LocalDateTime.now(), SECURITY, OrderOperation.BUY, 1.27, 400);
        Order orderBuy2 = OrderFactory.createLimitOrder(LocalDateTime.now(), SECURITY, OrderOperation.BUY, 1.26, 5050);
        Order orderBuy3 = OrderFactory.createLimitOrder(LocalDateTime.now(), SECURITY, OrderOperation.BUY, 1.25, 7200);
        Order orderBuy4 = OrderFactory.createLimitOrder(LocalDateTime.now(), SECURITY, OrderOperation.BUY, 1.23, 400);
        Order orderBuy5 = OrderFactory.createLimitOrder(LocalDateTime.now(), SECURITY, OrderOperation.BUY, 1.22, 400);

        Order orderSell1 = OrderFactory.createLimitOrder(LocalDateTime.now(), SECURITY, OrderOperation.SELL, 1.22, 3200);
        Order orderSell2 = OrderFactory.createLimitOrder(LocalDateTime.now(), SECURITY, OrderOperation.SELL, 1.23, 1400);
        Order orderSell3 = OrderFactory.createLimitOrder(LocalDateTime.now(), SECURITY, OrderOperation.SELL, 1.24, 300);
        Order orderSell4 = OrderFactory.createLimitOrder(LocalDateTime.now(), SECURITY, OrderOperation.SELL, 1.25, 35400);
        Order orderSell5 = OrderFactory.createLimitOrder(LocalDateTime.now(), SECURITY, OrderOperation.SELL, 1.26, 5550);
        Order orderSell6 = OrderFactory.createLimitOrder(LocalDateTime.now(), SECURITY, OrderOperation.SELL, 1.27, 4000);
        Order orderSell7 = OrderFactory.createLimitOrder(LocalDateTime.now(), SECURITY, OrderOperation.SELL, 1.28, 700);

        orderBook.enqueueOrderForDelayedProcessing(orderBuy1);
        orderBook.enqueueOrderForDelayedProcessing(orderBuy2);
        orderBook.enqueueOrderForDelayedProcessing(orderBuy3);
        orderBook.enqueueOrderForDelayedProcessing(orderBuy4);
        orderBook.enqueueOrderForDelayedProcessing(orderBuy5);

        orderBook.enqueueOrderForDelayedProcessing(orderSell1);
        orderBook.enqueueOrderForDelayedProcessing(orderSell2);
        orderBook.enqueueOrderForDelayedProcessing(orderSell3);
        orderBook.enqueueOrderForDelayedProcessing(orderSell4);
        orderBook.enqueueOrderForDelayedProcessing(orderSell5);
        orderBook.enqueueOrderForDelayedProcessing(orderSell6);
        orderBook.enqueueOrderForDelayedProcessing(orderSell7);

        FixRecord fixRecord = orderBook.fixAllOrders();
        assertThat(fixRecord.fixPrice()).isEqualTo(orderBuy2.getPrice());
        assertThat(fixRecord.fixVolume()).isEqualTo(orderBuy2.getSize());
    }

    @Test
    public void installOrdersInQueueTest() {
        setupNonMatchingOrders();
        orderBook.enqueueOrderForDelayedProcessing(OrderFactory.createMarkerOrder(LocalDateTime.now(), SECURITY, OrderOperation.BUY, 30));

        orderBook.fixAllOrders();

        List<Object[]> sellObjects = orderBook.sells.entrySet().stream()
            .map(entry -> new Object[]{entry.getKey(), entry.getValue().getLimitPrice(), entry.getValue().getTotalVolume()}).collect(
                Collectors.toList());

        assertThat(sellObjects).containsExactly(
            new Object[]{BigDecimal.valueOf(1.2640), BigDecimal.valueOf(1.2640), 220 - 15},
            new Object[]{BigDecimal.valueOf(1.2645), BigDecimal.valueOf(1.2645), 35},
            new Object[]{BigDecimal.valueOf(1.2655), BigDecimal.valueOf(1.2655), 14},
            new Object[]{BigDecimal.valueOf(1.2701), BigDecimal.valueOf(1.2701), 15000},
            new Object[]{BigDecimal.valueOf(1.2890), BigDecimal.valueOf(1.2890), 205000}
        );

    }

    @Test
    public void checkMarketOrderConsumedByTopLevelTest() {
        setupNonMatchingOrders();

        Order toMatch = OrderFactory.createMarkerOrder(LocalDateTime.now(), SECURITY, OrderOperation.BUY, 15);
        orderBook.processOrderImmediately(toMatch);

        List<Object[]> sellObjects = orderBook.sells.entrySet().stream()
            .map(entry -> new Object[]{entry.getKey(), entry.getValue().getLimitPrice(), entry.getValue().getTotalVolume()}).collect(
                Collectors.toList());

        assertThat(sellObjects).containsExactly(
            new Object[]{BigDecimal.valueOf(1.2640), BigDecimal.valueOf(1.2640), 220},
            new Object[]{BigDecimal.valueOf(1.2645), BigDecimal.valueOf(1.2645), 35},
            new Object[]{BigDecimal.valueOf(1.2655), BigDecimal.valueOf(1.2655), 14},
            new Object[]{BigDecimal.valueOf(1.2701), BigDecimal.valueOf(1.2701), 15000},
            new Object[]{BigDecimal.valueOf(1.2890), BigDecimal.valueOf(1.2890), 205000}
        );

        assertThat(
            trades.stream().filter(trade -> trade.getState() == State.FILLED).filter(trade -> trade.getOperation() == OrderOperation.SELL)
                .filter(trade -> trade.getFilledVolume() == 15)
                .map(Trade::getOrderId).findAny().orElse(-1)).isEqualTo(orderSell1.getOrderId());

        assertThat(
            trades.stream().filter(trade -> trade.getState() == State.FILLED).filter(trade -> trade.getOperation() == OrderOperation.BUY)
                .filter(trade -> trade.getFilledVolume() == 15)
                .map(Trade::getOrderId).findAny().orElse(-1)).isEqualTo(toMatch.getOrderId());


    }

    @Test
    public void checkMarketOrderConsumedPartiallyBy2TopLevelTest() {
        setupNonMatchingOrders();

        Order toMatch = OrderFactory.createMarkerOrder(LocalDateTime.now(), SECURITY, OrderOperation.BUY, 30);
        orderBook.processOrderImmediately(toMatch);

        List<Object[]> sellObjects = orderBook.sells.entrySet().stream()
            .map(entry -> new Object[]{entry.getKey(), entry.getValue().getLimitPrice(), entry.getValue().getTotalVolume()}).collect(
                Collectors.toList());

        assertThat(sellObjects).containsExactly(
            new Object[]{BigDecimal.valueOf(1.2640), BigDecimal.valueOf(1.2640), 220 - 15},
            new Object[]{BigDecimal.valueOf(1.2645), BigDecimal.valueOf(1.2645), 35},
            new Object[]{BigDecimal.valueOf(1.2655), BigDecimal.valueOf(1.2655), 14},
            new Object[]{BigDecimal.valueOf(1.2701), BigDecimal.valueOf(1.2701), 15000},
            new Object[]{BigDecimal.valueOf(1.2890), BigDecimal.valueOf(1.2890), 205000}
        );

        assertThat(
            trades.stream().filter(trade -> trade.getState() == State.FILLED).filter(trade -> trade.getOperation() == OrderOperation.SELL)
                .filter(trade -> trade.getFilledVolume() == orderSell1.getSize())
                .map(Trade::getOrderId).findAny().orElse(-1)).isEqualTo(orderSell1.getOrderId());

        assertThat(
            trades.stream().filter(trade -> trade.getState() == State.PARTIALLY_FILLED).filter(trade -> trade.getOperation() == OrderOperation.SELL)
                .filter(trade -> trade.getFilledVolume() == orderSell1.getSize())
                .map(Trade::getOrderId).findAny().orElse(-1)).isEqualTo(orderSell2.getOrderId());

        Trade firstBuyFill = trades.stream().filter(trade -> trade.getState() == State.PARTIALLY_FILLED)
            .filter(trade -> trade.getOperation() == OrderOperation.BUY)
            .filter(trade -> trade.getFilledVolume() == orderSell1.getSize()).findFirst().orElse(null);
        assertThat(firstBuyFill.getOrderId()).isEqualTo(toMatch.getOrderId());

        assertThat(
            trades.stream().filter(trade -> trade.getState() == State.FILLED).filter(trade -> trade.getOperation() == OrderOperation.BUY)
                .filter(trade -> trade.getFilledVolume() == toMatch.getSize() - firstBuyFill.getFilledVolume())
                .map(Trade::getOrderId).findAny().orElse(-1)).isEqualTo(toMatch.getOrderId());
    }

    @Test
    public void checkCancelFromSingleOrderLevelTest() {
        setupNonMatchingOrders();
        orderBook.cancel(orderBuy2.getOrderId());

        List<Object[]> buyObjects = orderBook.buys.entrySet().stream()
            .map(entry -> new Object[]{entry.getKey(), entry.getValue().getLimitPrice(), entry.getValue().getTotalVolume()}).collect(
                Collectors.toList());

        assertThat(buyObjects).containsExactly(
            new Object[]{BigDecimal.valueOf(1.2221), BigDecimal.valueOf(1.2221), 205},
            new Object[]{BigDecimal.valueOf(1.2370), BigDecimal.valueOf(1.2370), 15},
            new Object[]{BigDecimal.valueOf(1.2480), BigDecimal.valueOf(1.2480), 14000},
            new Object[]{BigDecimal.valueOf(1.2501), BigDecimal.valueOf(1.2501), 350},
            new Object[]{BigDecimal.valueOf(1.2520), BigDecimal.valueOf(1.2520), 10}
        );

        assertThat(orderBook.limitDictBuys).doesNotContainKey(BigDecimal.valueOf(1.2510));

    }

    @Test
    public void checkCancelFromMultipleOrderLevelTest() {
        setupNonMatchingOrders();

        Order orderBuy4_1 = OrderFactory.createLimitOrder(LocalDateTime.now(), SECURITY, OrderOperation.BUY, 1.2480, 1500);
        Order orderBuy4_2 = OrderFactory.createLimitOrder(LocalDateTime.now(), SECURITY, OrderOperation.BUY, 1.2480, 2000);

        orderBook.processOrderImmediately(orderBuy4_1);
        orderBook.processOrderImmediately(orderBuy4_2);

        Limit limit = orderBook.limitDictBuys.get(orderBuy4.getPrice());
        assertThat(limit).isNotNull();
        assertThat(limit.getTotalVolume()).isEqualTo(orderBuy4.getSize() + orderBuy4_1.getSize() + orderBuy4_2.getSize());
        assertThat(limit.getOrders().keySet()).containsExactly(orderBuy4.getOrderId(), orderBuy4_1.getOrderId(), orderBuy4_2.getOrderId());

        orderBook.cancel(orderBuy4_1.getOrderId());
        assertThat(limit.getTotalVolume()).isEqualTo(orderBuy4.getSize() + orderBuy4_2.getSize());
        assertThat(limit.getOrders().keySet()).containsExactly(orderBuy4.getOrderId(), orderBuy4_2.getOrderId());
    }

    @Test
    public void checkTwoMatchingLimitOrdersTest() {

        Order orderBuy = OrderFactory.createLimitOrder(LocalDateTime.now(), SECURITY, OrderOperation.BUY, 1.25, 1);
        Order orderSell = OrderFactory.createLimitOrder(LocalDateTime.now(), SECURITY, OrderOperation.SELL, 1.25, 1);

        orderBook.processOrderImmediately(orderBuy);
        orderBook.processOrderImmediately(orderSell);

        assertThat(trades.stream().
            filter(trade -> trade.getState().equals(State.FILLED)).
            filter(trade -> trade.getOperation().equals(OrderOperation.BUY))
            .filter(trade -> trade.getOrderId() == orderBuy.getOrderId()).count()).isEqualTo(1);

        assertThat(trades.stream().
            filter(trade -> trade.getState().equals(State.FILLED)).
            filter(trade -> trade.getOperation().equals(OrderOperation.SELL))
            .filter(trade -> trade.getOrderId() == orderSell.getOrderId()).count()).isEqualTo(1);

    }

    @Test
    public void check1PartialBuyLimitConsumedBy3TopLevelTest() {
        setupNonMatchingOrders();

        int originalVolume = 35 + 220 + 15 + 1000;
        Order toMatch = OrderFactory.createLimitOrder(LocalDateTime.now(), SECURITY, OrderOperation.BUY, 1.2645, originalVolume);
        orderBook.processOrderImmediately(toMatch);

        List<Object[]> buyObjects = orderBook.buys.entrySet().stream()
            .map(entry -> new Object[]{entry.getKey(), entry.getValue().getLimitPrice(), entry.getValue().getTotalVolume()}).collect(
                Collectors.toList());

        assertThat(buyObjects).containsExactly(
            new Object[]{BigDecimal.valueOf(1.2221), BigDecimal.valueOf(1.2221), 205},
            new Object[]{BigDecimal.valueOf(1.2370), BigDecimal.valueOf(1.2370), 15},
            new Object[]{BigDecimal.valueOf(1.2480), BigDecimal.valueOf(1.2480), 14000},
            new Object[]{BigDecimal.valueOf(1.2501), BigDecimal.valueOf(1.2501), 350},
            new Object[]{BigDecimal.valueOf(1.2510), BigDecimal.valueOf(1.2510), 200},
            new Object[]{BigDecimal.valueOf(1.2520), BigDecimal.valueOf(1.2520), 10},
            new Object[]{BigDecimal.valueOf(1.2645), BigDecimal.valueOf(1.2645), originalVolume - 15 - 220 - 35}
        );

        List<Object[]> sellObjects = orderBook.sells.entrySet().stream()
            .map(entry -> new Object[]{entry.getKey(), entry.getValue().getLimitPrice(), entry.getValue().getTotalVolume()}).collect(
                Collectors.toList());

        assertThat(sellObjects).containsExactly(
            new Object[]{BigDecimal.valueOf(1.2655), BigDecimal.valueOf(1.2655), 14},
            new Object[]{BigDecimal.valueOf(1.2701), BigDecimal.valueOf(1.2701), 15000},
            new Object[]{BigDecimal.valueOf(1.2890), BigDecimal.valueOf(1.2890), 205000}
        );

        assertThat(trades.stream().filter(trade -> trade.getState() == State.INSTALLED).map(Trade::getOrderId))
            .containsExactlyInAnyOrder(orderBuy1.getOrderId(), orderBuy2.getOrderId(), orderBuy3.getOrderId(), orderBuy4.getOrderId(),
                orderBuy5.getOrderId(), orderBuy6.getOrderId(),
                orderSell1.getOrderId(), orderSell2.getOrderId(), orderSell3.getOrderId(), orderSell4.getOrderId(), orderSell5.getOrderId(),
                orderSell6.getOrderId(), toMatch.getOrderId());

        assertThat(trades.stream().filter(trade -> trade.getState() == State.INSTALLED).filter(trade -> trade.getOrderId() == toMatch.getOrderId())
            .count()).isEqualTo(1);

        assertThat(
            trades.stream().filter(trade -> trade.getState() == State.PARTIALLY_FILLED).filter(trade -> trade.getOrderId() == toMatch.getOrderId())
                .map(Trade::getFilledVolume)).containsExactlyInAnyOrder(15, 220, 35);

        assertThat(
            trades.stream().filter(trade -> trade.getState() == State.FILLED).filter(trade -> trade.getOperation() == OrderOperation.SELL)
                .map(Trade::getFilledVolume)).containsExactlyInAnyOrder(15, 220, 35);

    }

    @Test
    public void check1PartialBuyLimitConsumedByTopLevelTest() {
        setupNonMatchingOrders();

        Order toMatch = OrderFactory.createLimitOrder(LocalDateTime.now(), SECURITY, OrderOperation.BUY, 1.2630, 10);
        orderBook.processOrderImmediately(toMatch);

        List<Object[]> buyObjects = orderBook.buys.entrySet().stream()
            .map(entry -> new Object[]{entry.getKey(), entry.getValue().getLimitPrice(), entry.getValue().getTotalVolume()}).collect(
                Collectors.toList());

        assertThat(buyObjects).containsExactly(
            new Object[]{BigDecimal.valueOf(1.2221), BigDecimal.valueOf(1.2221), 205},
            new Object[]{BigDecimal.valueOf(1.2370), BigDecimal.valueOf(1.2370), 15},
            new Object[]{BigDecimal.valueOf(1.2480), BigDecimal.valueOf(1.2480), 14000},
            new Object[]{BigDecimal.valueOf(1.2501), BigDecimal.valueOf(1.2501), 350},
            new Object[]{BigDecimal.valueOf(1.2510), BigDecimal.valueOf(1.2510), 200},
            new Object[]{BigDecimal.valueOf(1.2520), BigDecimal.valueOf(1.2520), 10}
        );

        List<Object[]> sellObjects = orderBook.sells.entrySet().stream()
            .map(entry -> new Object[]{entry.getKey(), entry.getValue().getLimitPrice(), entry.getValue().getTotalVolume()}).collect(
                Collectors.toList());

        assertThat(sellObjects).containsExactly(
            new Object[]{BigDecimal.valueOf(1.2630), BigDecimal.valueOf(1.2630), 5},
            new Object[]{BigDecimal.valueOf(1.2640), BigDecimal.valueOf(1.2640), 220},
            new Object[]{BigDecimal.valueOf(1.2645), BigDecimal.valueOf(1.2645), 35},
            new Object[]{BigDecimal.valueOf(1.2655), BigDecimal.valueOf(1.2655), 14},
            new Object[]{BigDecimal.valueOf(1.2701), BigDecimal.valueOf(1.2701), 15000},
            new Object[]{BigDecimal.valueOf(1.2890), BigDecimal.valueOf(1.2890), 205000}
        );

    }

    @Test
    public void check1FullBuyLimitConsumedByTopLevelTest() {
        setupNonMatchingOrders();

        Order toMatch = OrderFactory.createLimitOrder(LocalDateTime.now(), SECURITY, OrderOperation.BUY, 1.2630, 15);
        orderBook.processOrderImmediately(toMatch);

        List<Object[]> buyObjects = orderBook.buys.entrySet().stream()
            .map(entry -> new Object[]{entry.getKey(), entry.getValue().getLimitPrice(), entry.getValue().getTotalVolume()}).collect(
                Collectors.toList());

        assertThat(buyObjects).containsExactly(
            new Object[]{BigDecimal.valueOf(1.2221), BigDecimal.valueOf(1.2221), 205},
            new Object[]{BigDecimal.valueOf(1.2370), BigDecimal.valueOf(1.2370), 15},
            new Object[]{BigDecimal.valueOf(1.2480), BigDecimal.valueOf(1.2480), 14000},
            new Object[]{BigDecimal.valueOf(1.2501), BigDecimal.valueOf(1.2501), 350},
            new Object[]{BigDecimal.valueOf(1.2510), BigDecimal.valueOf(1.2510), 200},
            new Object[]{BigDecimal.valueOf(1.2520), BigDecimal.valueOf(1.2520), 10}
        );

        List<Object[]> sellObjects = orderBook.sells.entrySet().stream()
            .map(entry -> new Object[]{entry.getKey(), entry.getValue().getLimitPrice(), entry.getValue().getTotalVolume()}).collect(
                Collectors.toList());

        assertThat(sellObjects).containsExactly(
            new Object[]{BigDecimal.valueOf(1.2640), BigDecimal.valueOf(1.2640), 220},
            new Object[]{BigDecimal.valueOf(1.2645), BigDecimal.valueOf(1.2645), 35},
            new Object[]{BigDecimal.valueOf(1.2655), BigDecimal.valueOf(1.2655), 14},
            new Object[]{BigDecimal.valueOf(1.2701), BigDecimal.valueOf(1.2701), 15000},
            new Object[]{BigDecimal.valueOf(1.2890), BigDecimal.valueOf(1.2890), 205000}
        );

    }

    private void setupNonMatchingOrders() {

        orderBook.processOrderImmediately(orderBuy1);
        orderBook.processOrderImmediately(orderBuy2);
        orderBook.processOrderImmediately(orderBuy3);
        orderBook.processOrderImmediately(orderBuy4);
        orderBook.processOrderImmediately(orderBuy5);
        orderBook.processOrderImmediately(orderBuy6);

        orderBook.processOrderImmediately(orderSell1);
        orderBook.processOrderImmediately(orderSell2);
        orderBook.processOrderImmediately(orderSell3);
        orderBook.processOrderImmediately(orderSell4);
        orderBook.processOrderImmediately(orderSell5);
        orderBook.processOrderImmediately(orderSell6);

        List<Object[]> buyObjects = orderBook.buys.entrySet().stream()
            .map(entry -> new Object[]{entry.getKey(), entry.getValue().getLimitPrice(), entry.getValue().getTotalVolume()}).collect(
                Collectors.toList());

        assertThat(buyObjects).containsExactly(
            new Object[]{BigDecimal.valueOf(1.2221), BigDecimal.valueOf(1.2221), 205},
            new Object[]{BigDecimal.valueOf(1.2370), BigDecimal.valueOf(1.2370), 15},
            new Object[]{BigDecimal.valueOf(1.2480), BigDecimal.valueOf(1.2480), 14000},
            new Object[]{BigDecimal.valueOf(1.2501), BigDecimal.valueOf(1.2501), 350},
            new Object[]{BigDecimal.valueOf(1.2510), BigDecimal.valueOf(1.2510), 200},
            new Object[]{BigDecimal.valueOf(1.2520), BigDecimal.valueOf(1.2520), 10}
        );

        assertThat(orderBook.limitDictBuys.keySet()).containsExactlyInAnyOrder(
            BigDecimal.valueOf(1.2221),
            BigDecimal.valueOf(1.2370),
            BigDecimal.valueOf(1.2480),
            BigDecimal.valueOf(1.2501),
            BigDecimal.valueOf(1.2510),
            BigDecimal.valueOf(1.2520)
        );

        List<Object[]> sellObjects = orderBook.sells.entrySet().stream()
            .map(entry -> new Object[]{entry.getKey(), entry.getValue().getLimitPrice(), entry.getValue().getTotalVolume()}).collect(
                Collectors.toList());

        assertThat(sellObjects).containsExactly(
            new Object[]{BigDecimal.valueOf(1.2630), BigDecimal.valueOf(1.2630), 15},
            new Object[]{BigDecimal.valueOf(1.2640), BigDecimal.valueOf(1.2640), 220},
            new Object[]{BigDecimal.valueOf(1.2645), BigDecimal.valueOf(1.2645), 35},
            new Object[]{BigDecimal.valueOf(1.2655), BigDecimal.valueOf(1.2655), 14},
            new Object[]{BigDecimal.valueOf(1.2701), BigDecimal.valueOf(1.2701), 15000},
            new Object[]{BigDecimal.valueOf(1.2890), BigDecimal.valueOf(1.2890), 205000}
        );

        assertThat(orderBook.limitDictSells.keySet()).containsExactlyInAnyOrder(
            BigDecimal.valueOf(1.2630),
            BigDecimal.valueOf(1.2640),
            BigDecimal.valueOf(1.2645),
            BigDecimal.valueOf(1.2655),
            BigDecimal.valueOf(1.2701),
            BigDecimal.valueOf(1.2890)
        );

    }
}