package com.luxoft.exchange.simulator.orderbook;

import static org.assertj.core.api.Assertions.assertThat;

import com.luxoft.exchange.simulator.orderbook.Trade.State;
import com.luxoft.exchange.simulator.orders.OrderImpl;
import com.luxoft.exchange.simulator.orders.OrderOperation;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class LimitTest {

    private static final BigDecimal LIMIT_PRICE = BigDecimal.valueOf(25.0);

    private static final String SECURITY = "DUMMY";

    private final List<Trade> trades = new ArrayList<>();

    private Limit limit;

    private OrderImpl order1;
    private OrderImpl order2;
    private OrderImpl order3;
    private OrderImpl order4;


    @BeforeEach
    void init() {
        trades.clear();
        limit = new Limit(LIMIT_PRICE);

        order1 = (OrderImpl) OrderFactory.createLimitOrder(LocalDateTime.now(), SECURITY, OrderOperation.SELL, LIMIT_PRICE, 2);
        order2 = (OrderImpl) OrderFactory.createLimitOrder(LocalDateTime.now(), SECURITY, OrderOperation.SELL, LIMIT_PRICE, 3);
        order3 = (OrderImpl) OrderFactory.createLimitOrder(LocalDateTime.now(), SECURITY, OrderOperation.SELL, LIMIT_PRICE, 4);
        order4 = (OrderImpl) OrderFactory.createLimitOrder(LocalDateTime.now(), SECURITY, OrderOperation.SELL, LIMIT_PRICE, 5);

        limit.put(order1);
        limit.put(order2);
        limit.put(order3);
        limit.put(order4);

        assertThat(limit.getOrders()).hasSize(4);
        assertThat(limit.getTotalVolume()).isEqualTo(order1.getSize() + order2.getSize() + order3.getSize() + order4.getSize());
    }

    @Test
    public void limitAllOrdersPartialMatchTest() {
        OrderImpl toMatch = (OrderImpl) OrderFactory.createLimitOrder(LocalDateTime.now(), SECURITY, OrderOperation.BUY, LIMIT_PRICE,
            order1.getSize() + order2.getSize() + order3.getSize() + order4.getSize() + 2);

        int volumeToFill = limit.matchToOrder(toMatch, trades::add);
        assertThat(limit.getTotalVolume()).isEqualTo(0);
        assertThat(volumeToFill).isEqualTo(2);

        assertThat(
            trades.stream().filter(trade -> trade.getState() == State.FILLED).filter(trade -> trade.getOperation() == order1.getOrderOperation())
                .filter(trade -> trade.getFilledVolume() == order1.getSize())
                .filter(trade -> trade.getOrderId() == order1.getOrderId()).count()).isEqualTo(1);

        assertThat(
            trades.stream().filter(trade -> trade.getState() == State.FILLED).filter(trade -> trade.getOperation() == order2.getOrderOperation())
                .filter(trade -> trade.getFilledVolume() == order2.getSize())
                .filter(trade -> trade.getOrderId() == order2.getOrderId()).count()).isEqualTo(1);

        assertThat(
            trades.stream().filter(trade -> trade.getState() == State.FILLED).filter(trade -> trade.getOperation() == order3.getOrderOperation())
                .filter(trade -> trade.getFilledVolume() == order3.getSize())
                .filter(trade -> trade.getOrderId() == order3.getOrderId()).count()).isEqualTo(1);

        assertThat(
            trades.stream().filter(trade -> trade.getState() == State.FILLED).filter(trade -> trade.getOperation() == order4.getOrderOperation())
                .filter(trade -> trade.getFilledVolume() == order4.getSize())
                .filter(trade -> trade.getOrderId() == order4.getOrderId()).count()).isEqualTo(1);

        assertThat(
            trades.stream().filter(trade -> trade.getState() == State.PARTIALLY_FILLED)
                .filter(trade -> trade.getOperation() == toMatch.getOrderOperation())
                .filter(trade -> trade.getFilledVolume() == order1.getSize())
                .filter(trade -> trade.getOrderId() == toMatch.getOrderId()).count()).isEqualTo(1);

        assertThat(
            trades.stream().filter(trade -> trade.getState() == State.PARTIALLY_FILLED)
                .filter(trade -> trade.getOperation() == toMatch.getOrderOperation())
                .filter(trade -> trade.getFilledVolume() == order2.getSize())
                .filter(trade -> trade.getOrderId() == toMatch.getOrderId()).count()).isEqualTo(1);

        assertThat(
            trades.stream().filter(trade -> trade.getState() == State.PARTIALLY_FILLED)
                .filter(trade -> trade.getOperation() == toMatch.getOrderOperation())
                .filter(trade -> trade.getFilledVolume() == order3.getSize())
                .filter(trade -> trade.getOrderId() == toMatch.getOrderId()).count()).isEqualTo(1);

        assertThat(
            trades.stream().filter(trade -> trade.getState() == State.PARTIALLY_FILLED)
                .filter(trade -> trade.getOperation() == toMatch.getOrderOperation())
                .filter(trade -> trade.getFilledVolume() == order4.getSize())
                .filter(trade -> trade.getOrderId() == toMatch.getOrderId()).count()).isEqualTo(1);

    }

    @Test
    public void limit1OrderPartialMatchTest() {
        OrderImpl toMatch = (OrderImpl) OrderFactory.createLimitOrder(LocalDateTime.now(), SECURITY, OrderOperation.BUY, LIMIT_PRICE, 1);
        int volumeToFill = limit.matchToOrder(toMatch, trades::add);
        assertThat(limit.getTotalVolume()).isEqualTo(order1.getSize() + order2.getSize() + order3.getSize() + order4.getSize() - toMatch.getSize());

        assertThat(volumeToFill).isEqualTo(0);

        assertThat(
            trades.stream().filter(trade -> trade.getState() == State.FILLED).filter(trade -> trade.getOperation() == toMatch.getOrderOperation())
                .filter(trade -> trade.getFilledVolume() == toMatch.getSize())
                .filter(trade -> trade.getOrderId() == toMatch.getOrderId()).count()).isEqualTo(1);

        assertThat(
            trades.stream().filter(trade -> trade.getState() == State.PARTIALLY_FILLED)
                .filter(trade -> trade.getOperation() == order1.getOrderOperation())
                .filter(trade -> trade.getFilledVolume() == toMatch.getSize())
                .filter(trade -> trade.getOrderId() == order1.getOrderId()).count()).isEqualTo(1);

        assertThat(order1.getRemainingSize()).isEqualTo(order1.getSize() - toMatch.getSize());
    }

    @Test
    public void limit1OrderFullMatchTest() {

        OrderImpl toMatch = (OrderImpl) OrderFactory.createLimitOrder(LocalDateTime.now(), SECURITY, OrderOperation.BUY, LIMIT_PRICE, 2);
        int volumeToFill = limit.matchToOrder(toMatch, trades::add);

        assertThat(volumeToFill).isEqualTo(0);
        assertThat(limit.getOrders().keySet()).containsExactly(order2.getOrderId(), order3.getOrderId(), order4.getOrderId());
        assertThat(limit.getTotalVolume()).isEqualTo(order2.getSize() + order3.getSize() + order4.getSize());

        assertThat(
            trades.stream().filter(trade -> trade.getState() == State.FILLED).filter(trade -> trade.getOperation() == toMatch.getOrderOperation())
                .filter(trade -> trade.getFilledVolume() == order1.getSize())
                .filter(trade -> trade.getOrderId() == toMatch.getOrderId()).count()).isEqualTo(1);
        assertThat(
            trades.stream().filter(trade -> trade.getState() == State.FILLED).filter(trade -> trade.getOperation() == order1.getOrderOperation())
                .filter(trade -> trade.getFilledVolume() == order1.getSize())
                .filter(trade -> trade.getOrderId() == order1.getOrderId()).count()).isEqualTo(1);
    }

    @Test
    public void limit2OrderFullMatchTest() {

        OrderImpl toMatch = (OrderImpl) OrderFactory.createLimitOrder(LocalDateTime.now(), SECURITY, OrderOperation.BUY, LIMIT_PRICE, 5);
        int volumeToFill = limit.matchToOrder(toMatch, trades::add);

        assertThat(volumeToFill).isEqualTo(0);
        assertThat(limit.getOrders().keySet()).containsExactly(order3.getOrderId(), order4.getOrderId());
        assertThat(limit.getTotalVolume()).isEqualTo(order3.getSize() + order4.getSize());

        assertThat(
            trades.stream().filter(trade -> trade.getState() == State.PARTIALLY_FILLED)
                .filter(trade -> trade.getOperation() == toMatch.getOrderOperation())
                .filter(trade -> trade.getFilledVolume() == order1.getSize())
                .filter(trade -> trade.getOrderId() == toMatch.getOrderId()).count()).isEqualTo(1);
        assertThat(
            trades.stream().filter(trade -> trade.getState() == State.FILLED).filter(trade -> trade.getOperation() == toMatch.getOrderOperation())
                .filter(trade -> trade.getFilledVolume() == order2.getSize())
                .filter(trade -> trade.getOrderId() == toMatch.getOrderId()).count()).isEqualTo(1);

        assertThat(
            trades.stream().filter(trade -> trade.getState() == State.FILLED).filter(trade -> trade.getOperation() == order1.getOrderOperation())
                .filter(trade -> trade.getFilledVolume() == order1.getSize())
                .filter(trade -> trade.getOrderId() == order1.getOrderId()).count()).isEqualTo(1);
        assertThat(
            trades.stream().filter(trade -> trade.getState() == State.FILLED).filter(trade -> trade.getOperation() == order2.getOrderOperation())
                .filter(trade -> trade.getFilledVolume() == order2.getSize())
                .filter(trade -> trade.getOrderId() == order2.getOrderId()).count()).isEqualTo(1);
    }

}