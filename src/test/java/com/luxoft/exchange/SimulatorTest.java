package com.luxoft.exchange;


import static org.assertj.core.api.Assertions.assertThat;

import com.luxoft.exchange.simulator.orderbook.FixRecord;
import com.luxoft.exchange.simulator.orderbook.Trade;
import com.luxoft.exchange.simulator.orderbook.Trade.State;
import com.luxoft.exchange.simulator.orders.Order;
import com.luxoft.exchange.simulator.orders.OrderOperation;
import com.luxoft.exchange.util.GeneralUtil;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;


class SimulatorTest {

    private static final String PREOPEN_DIR = Paths.get("src", "test", "resources", "preopening").toString();
    private static final String PRECLOSE_DIR = Paths.get("src", "test", "resources", "preclosing").toString();
    private static final String CONTINUOUS_DIR = Paths.get("src", "test", "resources", "continuous").toString();
    private static final String CONTINUOUS_DIR_DUM = Paths.get("src", "test", "resources", "continuous_DUM").toString();

    private final List<Trade> trades = new ArrayList<>();

    private Simulator simulator;

    @TempDir
    private File tempDir;


    @BeforeEach
    public void init() {
        trades.clear();
        simulator = new Simulator(trades::add);

    }


    @Test
    public void resolveAllSymbolsPreopenTest() throws IOException {
        Set<String> symbolsToVerify = simulator.resolveAllSymbols(PREOPEN_DIR, true);

        Set<String> symbolsToRead = GeneralUtil.readFilesFromDirectory(PREOPEN_DIR).stream()
            .flatMap(file -> {
                List<String[]> asPerFile = new ArrayList<>();
                Simulator.parseRecordsLines(file, asPerFile::add);
                return asPerFile.stream();
            }).map(GeneralUtil::createPrePostOrder)
            .map(Order::getSecurity)
            .collect(Collectors.toSet());

        assertThat(symbolsToVerify).isEqualTo(symbolsToRead);
    }

    @Test
    public void resolveAllSymbolsSessionTest() throws IOException {
        Set<String> symbolsToVerify = simulator.resolveAllSymbols(CONTINUOUS_DIR, false);

        Set<String> symbolsToRead = GeneralUtil.readFilesFromDirectory(CONTINUOUS_DIR).stream()
            .flatMap(file -> {
                List<String[]> asPerFile = new ArrayList<>();
                Simulator.parseRecordsLines(file, asPerFile::add);
                return asPerFile.stream();
            }).map(GeneralUtil::createRegularOrder)
            .map(Order::getSecurity)
            .collect(Collectors.toSet());

        assertThat(symbolsToVerify).isEqualTo(symbolsToRead);
    }

    @Test
    public void readFilesConcurrentlyPrePostOrdersVanillaTest() throws IOException {
        Queue<Order> target = new ConcurrentLinkedQueue<>();
        simulator.readFilesConcurrently(PREOPEN_DIR, GeneralUtil::createPrePostOrder, target::add);
        Set<Order> readInParallel = new HashSet<>(target);

        List<File> allPreOpeningFiles = GeneralUtil.readFilesFromDirectory(PREOPEN_DIR);
        Set<Order> ordersInFolder = allPreOpeningFiles.stream().flatMap(file -> {
            List<String[]> asPerFile = new ArrayList<>();
            Simulator.parseRecordsLines(file, asPerFile::add);
            return asPerFile.stream();
        }).map(GeneralUtil::createPrePostOrder).collect(Collectors.toCollection(HashSet::new));

        assertThat(ordersInFolder).hasSize(readInParallel.size());
        assertThat(ordersInFolder).isEqualTo(readInParallel);

    }

    @Test
    public void readFilesConcurrentlySessionOrdersVanillaTest() throws IOException {
        Queue<Order> target = new ConcurrentLinkedQueue<>();
        simulator.readFilesConcurrently(CONTINUOUS_DIR, GeneralUtil::createRegularOrder, target::add);
        Set<Order> readInParallel = new HashSet<>(target);

        List<File> allPreOpeningFiles = GeneralUtil.readFilesFromDirectory(CONTINUOUS_DIR);
        Set<Order> ordersInFolder = allPreOpeningFiles.stream().flatMap(file -> {
            List<String[]> asPerFile = new ArrayList<>();
            Simulator.parseRecordsLines(file, asPerFile::add);
            return asPerFile.stream();
        }).map(GeneralUtil::createRegularOrder).collect(Collectors.toCollection(HashSet::new));

        assertThat(ordersInFolder).hasSize(readInParallel.size());
        assertThat(ordersInFolder).isEqualTo(readInParallel);

    }

    @Test
    public void outSessionVanillaTest() throws IOException, ExecutionException {
        prepareOutSessionFile();
        List<FixRecord> fixRecords = simulator.outMarketSimulation(tempDir.getPath());

        assertThat(fixRecords.stream().filter(s -> s.security().equals("SNN")).map(FixRecord::fixVolume).findAny().orElse(-1)).isEqualTo(-1);
        assertThat(fixRecords.stream().filter(s -> s.security().equals("BRD")).filter(s -> s.fixPrice().equals(BigDecimal.valueOf(1.24)))
            .map(FixRecord::fixVolume).findAny().orElse(-1)).isEqualTo(1500);

        assertThat(trades).isNotEmpty();
    }

    @Test
    public void inSessionVanillaTest() throws IOException {
        simulator.marketSimulation(CONTINUOUS_DIR);

        Set<String> securities = GeneralUtil.readFilesFromDirectory(CONTINUOUS_DIR).stream()
            .flatMap(file -> {
                List<String[]> asPerFile = new ArrayList<>();
                Simulator.parseRecordsLines(file, asPerFile::add);
                return asPerFile.stream();
            }).map(GeneralUtil::createRegularOrder)
            .map(Order::getSecurity)
            .collect(Collectors.toSet());

        // Trades + remaining securities must match
        Set<String> orderBookSecurities = simulator.orderBookMap.keySet();
        Set<String> allSecurities = trades.stream().map(Trade::getSecurity).collect(Collectors.toSet());
        allSecurities.addAll(orderBookSecurities);

        assertThat(trades).isNotEmpty();
        assertThat(allSecurities).isEqualTo(securities);

    }

    @Test
    public void eachSymbolInOwnThreadTest() throws IOException {
        simulator.marketSimulation(CONTINUOUS_DIR);

        Map<String, Set<Long>> threadsMap = new HashMap<>();
        trades.forEach(trade -> threadsMap.computeIfAbsent(trade.getSecurity(), (k) -> new HashSet<>()).add(trade.getTid()));

        threadsMap.values().forEach(threadSet -> assertThat(threadSet).hasSize(1));
    }

    @Test
    // orders go in unpredictable sequence, therefore can test only LIMIT
    public void inSessionDUMLimitDedicatedFolderTest() throws IOException {

        simulator.marketSimulation(CONTINUOUS_DIR_DUM);

        /**
         DUM;Buy;7000;Limit;1.23
         DUM;Sell;1500;Limit;1.26
         DUM;Buy;4000;Limit;1.22
         DUM;Sell;1500;Limit;1.26
         DUM;Buy;4000;Limit;1.25
         DUM;Buy;2500;Limit;1.23
         DUM;Buy;4000;Limit;1.25
         DUM;Sell;1500;Limit;1.26
         DUM;Sell;1500;Limit;1.26
         DUM;Buy;4000;Limit;1.25
         DUM;Sell;2500;Limit;1.27
         DUM;Buy;3000;Limit;1.55

         ----

         {BigDecimal@2888} "1.26" -> {Limit@2889} "Limit(limitPrice=1.26, totalVolume=3000, orders={18=OrderImpl(id=18, dateTime=2023-05-14T10:20, security=DUM, type=LIMIT, operation=SELL, price=1.26, size=1500), 20=OrderImpl(id=20, dateTime=2023-05-14T11:15, security=DUM, type=LIMIT, operation=SELL, price=1.26, size=1500)})"
         {BigDecimal@2890} "1.27" -> {Limit@2891} "Limit(limitPrice=1.27, totalVolume=2500, orders={24=OrderImpl(id=24, dateTime=2023-05-14T12:15, security=DUM, type=LIMIT, operation=SELL, price=1.27, size=2500)})"

         {BigDecimal@3185} "1.22" -> {Limit@3186} "Limit(limitPrice=1.22, totalVolume=4000, orders={16=OrderImpl(id=16, dateTime=2023-05-14T09:50, security=DUM, type=LIMIT, operation=BUY, price=1.22, size=4000)})"
         {BigDecimal@3187} "1.23" -> {Limit@3188} "Limit(limitPrice=1.23, totalVolume=9500, orders={14=OrderImpl(id=14, dateTime=2023-05-14T09:46, security=DUM, type=LIMIT, operation=BUY, price=1.23, size=7000), 19=OrderImpl(id=19, dateTime=2023-05-14T10:10, security=DUM, type=LIMIT, operation=BUY, price=1.23, size=2500)})"
         {BigDecimal@3189} "1.25" -> {Limit@3190} "Limit(limitPrice=1.25, totalVolume=12000, orders={17=OrderImpl(id=17, dateTime=2023-05-14T10:02, security=DUM, type=LIMIT, operation=BUY, price=1.25, size=4000), 21=OrderImpl(id=21, dateTime=2023-05-14T10:12, security=DUM, type=LIMIT, operation=BUY, price=1.25, size=4000), 22=OrderImpl(id=22, dateTime=2023-05-14T12:15, security=DUM, type=LIMIT, operation=BUY, price=1.25, size=4000)})"

         0 = {Trade@2884} "Trade(security=DUM, state=INSTALLED, operation=BUY, orderId=13, price=1.23, filledVolume=-1, dateTime=2023-05-14T13:04:59.850950376, tid=20)"
         1 = {Trade@2885} "Trade(security=DUM, state=INSTALLED, operation=SELL, orderId=14, price=1.26, filledVolume=-1, dateTime=2023-05-14T13:04:59.851326, tid=20)"
         2 = {Trade@2886} "Trade(security=DUM, state=INSTALLED, operation=SELL, orderId=16, price=1.26, filledVolume=-1, dateTime=2023-05-14T13:04:59.851429091, tid=20)"
         3 = {Trade@2887} "Trade(security=DUM, state=INSTALLED, operation=BUY, orderId=15, price=1.22, filledVolume=-1, dateTime=2023-05-14T13:04:59.851517418, tid=20)"
         4 = {Trade@2888} "Trade(security=DUM, state=INSTALLED, operation=BUY, orderId=17, price=1.25, filledVolume=-1, dateTime=2023-05-14T13:04:59.851617856, tid=20)"
         5 = {Trade@2889} "Trade(security=DUM, state=INSTALLED, operation=SELL, orderId=18, price=1.26, filledVolume=-1, dateTime=2023-05-14T13:04:59.851706561, tid=20)"
         6 = {Trade@2890} "Trade(security=DUM, state=INSTALLED, operation=BUY, orderId=19, price=1.23, filledVolume=-1, dateTime=2023-05-14T13:04:59.851788433, tid=20)"
         7 = {Trade@2891} "Trade(security=DUM, state=INSTALLED, operation=SELL, orderId=20, price=1.26, filledVolume=-1, dateTime=2023-05-14T13:04:59.851872184, tid=20)"
         8 = {Trade@2892} "Trade(security=DUM, state=INSTALLED, operation=BUY, orderId=22, price=1.25, filledVolume=-1, dateTime=2023-05-14T13:04:59.851953216, tid=20)"
         9 = {Trade@2893} "Trade(security=DUM, state=INSTALLED, operation=BUY, orderId=21, price=1.25, filledVolume=-1, dateTime=2023-05-14T13:04:59.852037486, tid=20)"
         10 = {Trade@2894} "Trade(security=DUM, state=INSTALLED, operation=SELL, orderId=23, price=1.27, filledVolume=-1, dateTime=2023-05-14T13:04:59.852171101, tid=20)"
         11 = {Trade@2895} "Trade(security=DUM, state=PARTIALLY_FILLED, operation=BUY, orderId=24, price=1.26, filledVolume=1500, dateTime=2023-05-14T13:04:59.852293842, tid=20)"
         12 = {Trade@2896} "Trade(security=DUM, state=FILLED, operation=SELL, orderId=14, price=1.26, filledVolume=1500, dateTime=2023-05-14T13:04:59.852310063, tid=20)"
         13 = {Trade@2897} "Trade(security=DUM, state=FILLED, operation=BUY, orderId=24, price=1.26, filledVolume=1500, dateTime=2023-05-14T13:04:59.852329642, tid=20)"
         14 = {Trade@2898} "Trade(security=DUM, state=FILLED, operation=SELL, orderId=16, price=1.26, filledVolume=1500, dateTime=2023-05-14T13:04:59.852339068, tid=20)"

         */

        assertThat(trades.stream()
            .filter(trade -> trade.getState() == State.FILLED)
            .filter(trade -> trade.getOperation() == OrderOperation.SELL)
            .filter(trade -> trade.getPrice().equals(BigDecimal.valueOf(1.26)))
            .filter(trade -> trade.getFilledVolume() == 1500)
            .count()).isEqualTo(2);

        assertThat(trades.stream()
            .filter(trade -> trade.getState() == State.PARTIALLY_FILLED)
            .filter(trade -> trade.getOperation() == OrderOperation.BUY)
            .filter(trade -> trade.getPrice().equals(BigDecimal.valueOf(1.26)))
            .filter(trade -> trade.getFilledVolume() == 1500)
            .count()).isEqualTo(1);

        assertThat(trades.stream()
            .filter(trade -> trade.getState() == State.FILLED)
            .filter(trade -> trade.getOperation() == OrderOperation.BUY)
            .filter(trade -> trade.getPrice().equals(BigDecimal.valueOf(1.26)))
            .filter(trade -> trade.getFilledVolume() == 1500)
            .count()).isEqualTo(1);
    }

    @Test
    public void inSessionDUMLimitDedicatedCommonTest() throws IOException {

        simulator.marketSimulation(CONTINUOUS_DIR);

        assertThat(trades.stream()
            .filter(trade -> trade.getState() == State.FILLED)
            .filter(trade -> "DUM".equals(trade.getSecurity()))
            .filter(trade -> trade.getOperation() == OrderOperation.SELL)
            .filter(trade -> trade.getPrice().equals(BigDecimal.valueOf(1.26)))
            .filter(trade -> trade.getFilledVolume() == 1500)
            .count()).isEqualTo(2);

        assertThat(trades.stream()
            .filter(trade -> trade.getState() == State.PARTIALLY_FILLED)
            .filter(trade -> "DUM".equals(trade.getSecurity()))
            .filter(trade -> trade.getOperation() == OrderOperation.BUY)
            .filter(trade -> trade.getPrice().equals(BigDecimal.valueOf(1.26)))
            .filter(trade -> trade.getFilledVolume() == 1500)
            .count()).isEqualTo(1);

        assertThat(trades.stream()
            .filter(trade -> trade.getState() == State.FILLED)
            .filter(trade -> "DUM".equals(trade.getSecurity()))
            .filter(trade -> trade.getOperation() == OrderOperation.BUY)
            .filter(trade -> trade.getPrice().equals(BigDecimal.valueOf(1.26)))
            .filter(trade -> trade.getFilledVolume() == 1500)
            .count()).isEqualTo(1);
    }


    private void prepareOutSessionFile() throws IOException {
        String content = "BRD;B;4200;1.24\n"
            + "BRD;A;1500;1.24\n"
            + "BRD;A;500;1.25\n"
            + "SNN;A;500;1.25";

        FileUtils.write(new File(FilenameUtils.concat(tempDir.getPath(), "preopen1.txt")), content, StandardCharsets.UTF_8);
    }


}